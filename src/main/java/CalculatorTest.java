import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

public class CalculatorTest {

    static AppiumDriver<MobileElement> appiumDriver;
    // WebDriver webDriver;
    // AndroidDriver androidDriver;

    public static void main (String args[]) throws MalformedURLException {
        openCalculator();
    }

    public static void openCalculator() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","Galaxy M31");
        capabilities.setCapability("udid","R58N72ZMF1W");
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("platformVersion","11");

        capabilities.setCapability("appPackage","com.sec.android.app.popupcalculator");
        capabilities.setCapability("appActivity",".Calculator");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        appiumDriver = new AndroidDriver<MobileElement>(url, capabilities);

        System.out.println("Application started....");

        MobileElement three =  appiumDriver.findElement(By.id("com.sec.android.app.popupcalculator:id/calc_keypad_btn_03"));
        MobileElement plus =  appiumDriver.findElement(By.id("com.sec.android.app.popupcalculator:id/calc_keypad_btn_add"));
        MobileElement two =  appiumDriver.findElement(By.id("com.sec.android.app.popupcalculator:id/calc_keypad_btn_02"));
        MobileElement equal =  appiumDriver.findElement(By.id("com.sec.android.app.popupcalculator:id/calc_keypad_btn_equal"));
        MobileElement result =  appiumDriver.findElement(By.id("com.sec.android.app.popupcalculator:id/calc_edt_formula"));


        three.click();
        plus.click();
        two.click();
        equal.click();

        System.out.println("Result = " + result.getText());

        System.out.println(appiumDriver.getPageSource());


    }
}


